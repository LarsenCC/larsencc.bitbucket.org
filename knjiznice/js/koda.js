
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var getEhrID = []; //pomoč pri generiranju 3 pacientov

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * 
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  
  if (stPacienta == 1) {
    var ime = "Jože";
    var priimek = "Horvat";
    var spol = "MALE";
    var datumRojstva = "1990-12-12";
  } else if (stPacienta == 2) {
    var ime = "Marija";
    var priimek = "Sveta";
    var spol = "FEMALE";
    var datumRojstva = "1948-04-12";
  } else {
    var ime = "Žiga";
    var priimek = "Fant";
    var spol = "MALE";
    var datumRojstva = "2005-03-26";
  }
  
  $("#ime").val(ime);
  $("#priimek").val(priimek);
  $("#datumRojstva").val(datumRojstva);
	
  ustvariPacienta(spol);
}

//zgeneriraj 1, 2 in 3
function zgenerirajTriUporabnike() {
  for (var i = 1; i < 4; i++) { 
    generirajPodatke(i);
  }
}

function addOption(ehrId, name, id) {
  var select = document.getElementById(id);
  var opt = document.createElement("option");
  opt.value = ehrId;
  opt.innerHTML = name;
  select.appendChild(opt);
}

function izbiraPacienta(id) {
  if (id != 3) {
    var ehrId = id == 1 ? $("#pacienti").val() : $("#pacienti2").val();
  } else {
    $("#infoIzbranega").html("");
    var ehrId = $("#ehrId2").val();
  }
  
  if (id == 1) {
    $("#ehrId").val(ehrId);
    $("#afterTextMeritve").html("");
  } else {
    document.getElementById("prof").innerHTML = "";
    $("#ehrId2").val(ehrId);
  }
  
  console.log("Izbrali ste: " + ehrId);
  
  if (id == 2) {
    //remove children
    $("#itm").html("???");
    $("#temp").html("???");
    $("#kisik").html("???");
    $("#rezultat").html("???");
     
    var myNode = document.getElementById("meritve");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    var opt = document.createElement("option");
    opt.value = "";
    opt.innerHTML = "izberite meritev";
    myNode.appendChild(opt);
    
    document.getElementById("meritve").disabled = true
  }
  
  if (!ehrId && id == 2) {
    $("#infoIzbranega").html("");
  } else if (ehrId) {
    $.ajax({
  		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
  		type: 'GET',
  		headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
        var datum = party.dateOfBirth.split("-");
        if (id >= 2) {
          $("#infoIzbranega").html(party.firstNames + " " + party.lastNames + ' (' + (party.gender == "MALE" ? "Moški" : "Ženska") + ') rojen/a ' + datum[2] + "." + datum[1] + "." + datum[0]);
        }
  		},
  		error: function(err) {
        console.log(err);
        $("#afterText").html("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
  		}
  	});
  }

}

function ustvariPacienta(spol) {
  var ime = $("#ime").val();
  var priimek = $("#priimek").val();
  var SPOL = spol == null ? $("#spol").val() : spol;
  var datumRojstva = $("#datumRojstva").val();
  
  if (!ime || !priimek || !SPOL || !datumRojstva) {
    $("#afterText").html("Vnesite vsa polja!");
  } else {
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          gender: SPOL,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        
        $("#ehrId").val(ehrId);
        
        
        //ehrIdsCreated.push(ehrId);
        addOption(ehrId, (ime + " " + priimek), "pacienti");
        addOption(ehrId, (ime + " " + priimek), "pacienti2");
        console.log("CREATED: " + ehrId);
        
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              //$("#afterText").html("Ustvarjeno: " + party.meta.href);
              $("#afterText").html(spol == null ? ('Ustvarjeno: ' + ime + ' ' + priimek + " (" + ehrId + ")") : "");
              
              //ce je generirano avtomatsko
              if (spol != null) {
                getEhrID.push(ehrId);
                nastaviVrednostiMeritev(getEhrID.length, ehrId);
                
                if (getEhrID.length == 3) {
                  var ids = "";
                  for (var i = 0; i < 3; i++) {
                    ids = ids + getEhrID[i] + '\n';
                  }
                  alert('3 pacienti generirani!\n' + ids + 'Najdete jih v meniju pod "IZBERITE PACIENTA"');
                  getEhrID = [];
                }
              }
                
            }
          },
          error: function(err) {
            console.log(err);
            $("#afterText").html("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
          }
          
        });
      }
    });
    
    $("#ime").val("");
    $("#priimek").val("");
    $("#spol").val("");
    $("#datumRojstva").val("");
  }
  
}

function dodajMeritve() {
  var ehrId = $("#ehrId").val();
  var datumInUra = $("#datumInUra").val();
  var telesnaVisina = $("#telesnaVisina").val();
  var telesnaTeza = $("#telesnaTeza").val();
  var telesnaTemperatura = $("#telesnaTemperatura").val();
  var nasicenostKrviSKisikom = $("#nasicenostKrvi").val();
  var zdravnik = $("#zdravnik").val();
  
  if (!ehrId || ehrId.trim().length == 0 || !datumInUra || !telesnaVisina || !telesnaTeza || !telesnaTemperatura || !nasicenostKrviSKisikom) {
		$("#afterTextMeritve").html("Prosim vnesite zahtevane podatke!");
	} else {
	  var meritve = {
	    "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": datumInUra,
	    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
	    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
	   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
	    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
	    "vital_signs/blood_pressure/any_event/diastolic": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: zdravnik
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(meritve),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#afterTextMeritve").html("Meritve uspešno dodane!");
        
        $("#datumInUra").val("");
      	$("#telesnaVisina").val("");
      	$("#telesnaTeza").val("");
      	$("#telesnaTemperatura").val("");
      	$("#nasicenostKrvi").val("");
      	$("#zdravnik").val("");
      	$("#afterTextMeritve").val("");
      },
      error: function(err) {
      	$("#afterTextMeritve").html("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
	
}

//pomožna funkcija za random genereiranje meritev treh zgeneriranih pacientov
function nastaviVrednostiMeritev(iteracija, ehrId) {
  if (iteracija == 1) {
    console.log("DODAJAM MERITVE ZA 1");
    for (var i = 10; i < 15; i++) {
      $("#ehrId").val(ehrId);
      $("#datumInUra").val('2019-05-' + i + 'T12:00');
    	$("#telesnaVisina").val("185");
    	$("#telesnaTeza").val(75 + Math.floor(Math.random()*3));
    	$("#telesnaTemperatura").val(parseFloat(Math.random().toFixed(1)) + 36);
    	$("#nasicenostKrvi").val(95 + Math.floor(Math.random()*5));
    	$("#zdravnik").val("Zdenka Sirček");
      dodajMeritve();                 
    }
  } else if (iteracija == 2) {
    console.log("DODAJAM MERITVE ZA 2");
    for (var i = 10; i < 15; i++) {
      $("#ehrId").val(ehrId);
      $("#datumInUra").val('2019-05-' + i + 'T12:00');
    	$("#telesnaVisina").val("164");
    	$("#telesnaTeza").val(65 + Math.floor(Math.random()*3));
    	$("#telesnaTemperatura").val(parseFloat(Math.random().toFixed(1)) + 38);
    	$("#nasicenostKrvi").val(70 + Math.floor(Math.random()*10));
    	$("#zdravnik").val("Zdenka Sirček");
      dodajMeritve();                 
    }
  } else {
    console.log("DODAJAM MERITVE ZA 3");
    for (var i = 10; i < 15; i++) {
        $("#ehrId").val(ehrId);
        $("#datumInUra").val('2019-05-' + i + 'T12:00');
      	$("#telesnaVisina").val("151");
      	$("#telesnaTeza").val(i < 14 ? (45 + Math.floor(Math.random()*3)) : (39 + Math.floor(Math.random()*3)));
      	$("#telesnaTemperatura").val(i < 14 ? (parseFloat(Math.random().toFixed(1)) + 36) : (39 + Math.floor(Math.random()*3)));
      	$("#nasicenostKrvi").val(i < 14 ? (95 + Math.floor(Math.random()*5)) : (85 + Math.floor(Math.random()*5)));
      	$("#zdravnik").val("Zdenka Sirček");
        dodajMeritve(); 
    }
  }
  
  $("#datumInUra").val("");
	$("#telesnaVisina").val("");
	$("#telesnaTeza").val("");
	$("#telesnaTemperatura").val("");
	$("#nasicenostKrvi").val("");
	$("#zdravnik").val("");
	$("#afterTextMeritve").val("");
  
}

//API - dodaten vir
function pridobiPodatkeDrzave(callback) {
  var podatki;
  var url = 'knjiznice/json/podatki.json';
  
  var xobj = new XMLHttpRequest();
  xobj.open("GET", url, true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
      var json = JSON.parse(xobj.responseText);
      callback(json);
    }
  };
  xobj.send(null);
}

function pridobiStarostZensk(json) {
  var zenske = [];
  for (var i = 0; i < json.data.length; i++) {
    zenske.push([json.data[i].country, parseFloat(json.data[i].ages.FEMALE)])
  }
  return zenske;
}

function pridobiStarostMoskih(json) {
  var moski = [];
  for (var i = 0; i < json.data.length; i++) {
    moski.push([json.data[i].country, parseFloat(json.data[i].ages.MALE)])
  }
  return moski;
}

window.addEventListener('load', function () {
  izrisiGraf();
});

//funkcija za izracun Indeksa Telesne Mase
function izracunajITM(teza, visina) {
  return (teza / (parseFloat(visina / 100) * parseFloat(visina / 100))).toFixed(2);
}

function odstotekZaPrezivetje(letoRojstva, itm, temp, nas, povpStarost) {
  var today = new Date();
  var leto = today.getFullYear();
  var starost = leto - letoRojstva;
  var moznost = 0;
  var razTemp = Math.abs(temp - 36.5) > 3 ? parseFloat(Math.abs(temp - 36.5) * 3).toFixed(2) : parseFloat(Math.abs(temp - 36.5)).toFixed(2);
  var razItm = Math.abs(itm - 22) > 5 ? parseFloat(Math.abs(itm - 22) * 3).toFixed(2) : parseFloat(Math.abs(itm - 22)).toFixed(2);
  
  if (starost >= povpStarost) {
    return 100;
  } else {
    moznost = parseFloat(parseInt(nas) + 5 - (parseFloat(povpStarost / starost).toFixed(2) * 3) - razTemp - razItm).toFixed(1);
    if (moznost >= 100) {
      return 100;
    } else if (moznost <= 0) {
      return 0;
    } else {
      return moznost;
    }
    
  }
}

//moznost dosega povprecne starosti v %
function izracunajMoznosti() {
  pridobiPodatkeDrzave(function(json) {
    var iDrzave = $("#drzave").val();
    var data = json.data;
    var ehrId = $("#ehrId2").val();
    
    if (!iDrzave || !ehrId) {
      $("#afterTextGrafi").html("Vnesite vse podatke!");
    } else if (!document.getElementById("meritve").disabled && $("#meritve").val()) {
      var splitted = $("#meritve").val().split(",");
      
      var itm = splitted[1];
      var t = splitted[2];
      var k = splitted[3];
      var letoRojstva = splitted[4];
      
      var split = document.getElementById("infoIzbranega").innerHTML.split("(");
      
      var spol = split[1].split(")");
      spol = spol[0];
      
      var rezultat = odstotekZaPrezivetje(letoRojstva, itm, t, k, (spol == "Ženska" ? data[$("#drzave").val()].ages.FEMALE : data[$("#drzave").val()].ages.MALE) );
      
      $("#itm").html(itm);
      $("#temp").html(t + "°C");
      $("#kisik").html(k + "%");
      $("#rezultat").html(rezultat + "%");
      
    } else if (document.getElementById("meritve").disabled) {
      
      var myNode = document.getElementById("meritve");
      while (myNode.firstChild) {
          myNode.removeChild(myNode.firstChild);
      }
      
      pridobiTezo(function(teze) {
        pridobiVisino(function(visine) {
          pridobiTemperaturo(function(temp) {
            pridobiKisik(function(kisik) {
              var stPod = teze.length;
              var vseSkupaj = [];
              var itm = izracunajITM(teze[0].weight, visine[0].height);
              var t = temp[0].temperature;
              var k = kisik[0].diastolic;
              
              var split = document.getElementById("infoIzbranega").innerHTML.split(".");
              var letoRojstva = split[2];
              split = document.getElementById("infoIzbranega").innerHTML.split("(");
              
              var spol = split[1].split(")");
              spol = spol[0];
              //console.log(letoRojstva);
              var rezultat = odstotekZaPrezivetje(letoRojstva, itm, t, k, (spol == "Ženska" ? data[$("#drzave").val()].ages.FEMALE : data[$("#drzave").val()].ages.MALE) );
              var rezultati = [];
              
              for (var i = 0; i < stPod; i++) {
                rezultati.push(odstotekZaPrezivetje(letoRojstva, izracunajITM(teze[i].weight, visine[i].height), temp[i].temperature, kisik[i].diastolic, (spol == "Ženska" ? data[$("#drzave").val()].ages.FEMALE : data[$("#drzave").val()].ages.MALE) ));
                vseSkupaj.push([teze[i].time, izracunajITM(teze[i].weight, visine[i].height), temp[i].temperature, kisik[i].diastolic, letoRojstva]);
              }
              
              document.getElementById("meritve").disabled = false;
              for (var i = 0; i < stPod; i++) {
                var select = document.getElementById("meritve");
                var opt = document.createElement("option");
                opt.value = vseSkupaj[i];
                
                var datumInUra =  vseSkupaj[i][0].split("T");
                var ura = datumInUra[1].split(":");
                var datum = datumInUra[0].split("-");
                
                opt.innerHTML = datum[2] + "." + datum[1] + "." + datum[0] + ' (' + ura[0] + ":" + ura[1] + ')';
                select.appendChild(opt);
              }
              
              $("#itm").html(itm);
              $("#temp").html(t + "°C");
              $("#kisik").html(k + "%");
              $("#rezultat").html(rezultat + "%");

              izrisiProf(rezultati);
            });
          });
        });
      });
    }
  });
}

function izpisiPodrobnostiDrzave() {
  pridobiPodatkeDrzave(function(json) {
    var indexDrzave = $("#drzave").val();
    
    if (indexDrzave) {
      //console.log("DONEEEEEEEEE");
      var drzava = json.data[indexDrzave].country;
      var starostM = json.data[indexDrzave].ages.MALE;
      var starostF = json.data[indexDrzave].ages.FEMALE;
      $("#textGrafM").html('(' + indexDrzave + ') ' + drzava + ": " + starostM + " let");
      $("#textGrafF").html('(' + indexDrzave + ') ' + drzava + ": " + starostF + " let");  
    } else {
      $("#textGrafM").html("");
      $("#textGrafF").html("");
    }
    
    document.getElementById("meritve").disabled = true;
  });
}

//izrisi grafe za povprečne starosti po državah
function izrisiGraf() {
  pridobiPodatkeDrzave(function(json) {
    
    //stevilo podatkov
    var n = json.data.length;
    
    for (var i = 0; i < n; i++) {
      var select = document.getElementById("drzave");
      var opt = document.createElement("option");
      opt.value = i;
      opt.innerHTML = i + " - " + json.data[i].country;
      //console.log(json.data[i].country);
      select.appendChild(opt);
    }

  var margin = {top: 50, right: 50, bottom: 50, left: 50}
    , width = 500 //window.innerWidth - margin.left - margin.right // Use the window's width 
    , height = 250 //window.innerHeight - margin.top - margin.bottom; // Use the window's height
  
  //graf za moške
  var xScale = d3.scaleLinear().domain([0, n - 1]).range([0, width]);
  var yScale = d3.scaleLinear().domain([40, 100]).range([height, 0]);
  
  var line = d3.line()
    .x(function(d, i) {return xScale(i);})
    .y(function(d) {return yScale(d[1]);})
    .curve(d3.curveMonotoneX);

  var moski = pridobiStarostMoskih(json);
  
  var svg = d3.select("graf").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,"+height+")")
    .call(d3.axisBottom(xScale));
  svg.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(yScale));
  svg.append("path")
    .datum(moski)
    .attr("class", "line")
    .attr("d", line);
  
  //graf za ženske
  var xScale = d3.scaleLinear().domain([0, n - 1]).range([0, width]);
  var yScale = d3.scaleLinear().domain([40, 100]).range([height, 0]);
  
  var line = d3.line()
    .x(function(d, i) {return xScale(i);})
    .y(function(d) {return yScale(d[1]);})
    .curve(d3.curveMonotoneX);

  var zenske = pridobiStarostZensk(json);
  
  var svg1 = d3.select("graf1").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
  svg1.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,"+height+")")
    .call(d3.axisBottom(xScale));
  svg1.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(yScale));
  svg1.append("path")
    .datum(zenske)
    .attr("class", "lineF")
    .attr("d", line);
  });
}

//izriši graf iz rezultatov vseh meritev pacienta
function izrisiProf(meritve) {
  document.getElementById("prof").innerHTML = "";
  var n = meritve.length;
  
  var margin = {top: 50, right: 50, bottom: 50, left: 50}
    , width = 500 //window.innerWidth - margin.left - margin.right // Use the window's width 
    , height = 250 //window.innerHeight - margin.top - margin.bottom; // Use the window's height
    
  var xScale = d3.scaleLinear().domain([0, n - 1]).range([0, width]);
  var yScale = d3.scaleLinear().domain([0, 100]).range([height, 0]);
  
  var line = d3.line()
    .x(function(d, i) {return xScale(i);})
    .y(function(d) {return yScale(d);})
    .curve(d3.curveMonotoneX);
  
  var svg = d3.select("prof").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,"+height+")")
    .call(d3.axisBottom(xScale));
  svg.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(yScale));
  svg.append("path")
    .datum(meritve)
    .attr("class", "lineF")
    .attr("d", line);
}

//funkcije ki pridobijo podatke o bolnikih

//pridobi teze pacienta
function pridobiTezo(callback) {
  $("#afterTextGrafi").html("");
  var ehrId = $("#ehrId2").val();
  //console.log(ehrId);
  $.ajax({
      url: baseUrl + "/view/" + ehrId + "/weight",
      type: 'GET',
      headers: {
          "Authorization": getAuthorization()
      },
      success: function (res) {
        callback(res);
      },
      error: function(err) {
      	$("#afterTextGrafi").html("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
      }
  });
}

//pridobi visino pacienta
function pridobiVisino(callback) {
  $("#afterTextGrafi").html("");
  var ehrId = $("#ehrId2").val();
  $.ajax({
      url: baseUrl + "/view/" + ehrId + "/height",
      type: 'GET',
      headers: {
          "Authorization": getAuthorization()
      },
      success: function (res) {
        callback(res);
      },
      error: function(err) {
      	$("#afterTextGrafi").html("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
      }
  });
}

//pridobi temperature pacienta
function pridobiTemperaturo(callback) {
  $("#afterTextGrafi").html("");
  var ehrId = $("#ehrId2").val();
  $.ajax({
      url: baseUrl + "/view/" + ehrId + "/body_temperature",
      type: 'GET',
      headers: {
          "Authorization": getAuthorization()
      },
      success: function (res) {
        callback(res);
      },
      error: function(err) {
      	$("#afterTextGrafi").html("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
      }
  });
}

//pridobi kisika v krvi pacienta
function pridobiKisik(callback) {
  $("#afterTextGrafi").html("");
  var ehrId = $("#ehrId2").val();
  $.ajax({
      url: baseUrl + "/view/" + ehrId + "/blood_pressure",
      type: 'GET',
      headers: {
          "Authorization": getAuthorization()
      },
      success: function (res) {
        callback(res);
      },
      error: function(err) {
      	$("#afterTextGrafi").html("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
      }
  });
}